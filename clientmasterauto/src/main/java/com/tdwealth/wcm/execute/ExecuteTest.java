package com.tdwealth.wcm.execute;



import cucumber.api.CucumberOptions;

//import cucumber.api.junit.Cucumber;
/*import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;*/
import cucumber.api.testng.AbstractTestNGCucumberTests;



@CucumberOptions(
		glue= "com.tdwealth.wcm.stepdefinitions",
		plugin = {"com.cucumber.listener.ExtentCucumberFormatter:output/report.html"}
		,features = "gherkinfeature"
		)
public class ExecuteTest extends AbstractTestNGCucumberTests{
	
	
}

