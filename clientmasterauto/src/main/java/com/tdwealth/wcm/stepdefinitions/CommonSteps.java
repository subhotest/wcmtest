package com.tdwealth.wcm.stepdefinitions;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.*;


public class CommonSteps {

	
	@When("^test$")
	public void test() throws Throwable{
		System.out.println("ok");
	}
	
	@ContinueNextStepsFor(Throwable.class)
	@When("^hello$")
	public void hello() throws Throwable{
		throw new Exception("hello");
	}
	
}
